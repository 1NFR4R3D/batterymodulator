

import math

## Constants ##
tv = 120
tm = 12
te = 6

## User Input for Cell & Battery Parameters ##
print("Please enter the following input parameters - ")
cp = float(input("Enter number of parallel cells (number) - "))
cs = float(input("Enter number of series cells (number) - "))
cv = float(input("Enter peak cell voltage (Volts) - "))
cc = float(input("Enter peak cell capacity (Amp-Hr) - "))
cm = float(input("Enter cell weight (g) - "))
ce = float(input("Enter cell energy (Wh) - "))

## Series Unit Computation ##
print("\nSeries unit details - ")
uc = cc*cp
print("Capacity = " + str(uc) + "Ah")
um = cm*cp/1000
print("Mass = " + str(um) + "kg")
uv = cv
print("Voltage = " + str(uv) + "V")
ue = ce*cp*3.6/1000
print("Energy = " + str(ue) + "MJ")

## Module iterations ##
print("\nSize\t|Voltage|Mass\t|Energy\t|Number")
for i in range(1,int(cs)):
    if cs%i == 0:
        ms = i
        mn = int(cs/i)
        mv = round(ms*uv,2)
        mm = round(ms*um,2)
        me = round(ms*ue,2)
        if mv<tv:
            if mm<tm:
                if me<te:
                    print(str(ms) + "\t|" + str(mv) + "\t|" + str(mm) + "\t|" + str(me) + "\t|" + str(mn))
        #else:
            #print(str(ms) + "," + str(mv) + "," + str(mm) + "," + str(me))

print("\nDone!")
